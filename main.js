$(function () {

    const $access_key = '40e53846728e998a0be05be1d4e5a5d9'; // Token da api
    const $output = 'json'; // Tipo de Output da response


    //Função com uma expressão regular que verifica se a URL é uma URL válida
    function validateUrl(url) {
        var urlregex = new RegExp("^([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
        return urlregex.test(url);
    }


    $('#locate_website').on('click', function () { //Quando clicar no botão locate a Website


        var $query_site = $('#query_site').val(); //Vai pegar o texto


        $('.list-group-item').remove(); //Essa linha serve pra remover o que estava escrito na tabela antes de clicar no botão
        if (validateUrl($query_site) == true) { // Vai enviar a url digitada para verificação se retornar TRUE continua o código
            $("#query_site").css("background-color", "#006600"); // Adiciona uma cor verde para o input
            $("#query_site").css("color", "black"); // Adiciona uma cor preta para as letras
            $.ajax({ // Vai enviar uma requisição get para a API especificada nas opções
                url: 'http://api.ipstack.com/' + $query_site + '?access_key=' + $access_key + '&output=' + $output,
                dataType: "json",
                success: function (json) { // Se a requisição for sucedida, ela irá retornar os dados na sua response e vai fazer um append na lista no arquivo HTML com todos seus campos e valores
                    $('#locations').append($('<li>IP: ' + json.ip + '</li>').addClass("list-group-item"));
                    $('#locations').append($('<li>Country: ' + json.country_name + '</li>').addClass("list-group-item"));
                    $('#locations').append($('<li>Region: ' + json.region_name + '</li>').addClass("list-group-item"));
                    $('#locations').append($('<li>City: ' + json.city + '</li>').addClass("list-group-item"));
                    $('#locations').append($('<li>Zip: ' + json.zip + '</li>').addClass("list-group-item"));
                    $('#locations').append($('<li>Latitude: ' + json.latitude + '</li>').addClass("list-group-item"));
                    $('#locations').append($('<li>Longitude: ' + json.longitude + '</li>').addClass("list-group-item"));
                }
            });
        } else { // Se caso a função retornar FALSE é por que a url está inválida
            $("#query_site").css("background-color", "#b20000"); // Adicionar uma cor vermelha para o input
            $("#query_site").addClass("placeholder-color"); // Muda a cor do place holder
            $("#query_site").attr("placeholder", "Invalid URL"); // Texto do placeholder
        }


    });

    
    $('#locate_me').on('click', function () { // Quando clicar no botão Get My Location 
        $('.list-group-item').remove();
        $.ajax({ // Apartir daqui o código vai buscar na API os dados de quem ta fazendo a requisição e irá retorná-los e adicioná-los seus respectivos campos e valores na tabela no documento HTML
            url: 'http://api.ipstack.com/check?access_key=' + $access_key + '&output=' + $output,
            dataType: "json",
            success: function (json) {
                $('#locations').append($('<li>IP: ' + json.ip + '</li>').addClass("list-group-item"));
                $('#locations').append($('<li>Country: ' + json.country_name + '</li>').addClass("list-group-item"));
                $('#locations').append($('<li>Region: ' + json.region_name + '</li>').addClass("list-group-item"));
                $('#locations').append($('<li>City: ' + json.city + '</li>').addClass("list-group-item"));
                $('#locations').append($('<li>Zip: ' + json.zip + '</li>').addClass("list-group-item"));
                $('#locations').append($('<li>Latitude: ' + json.latitude + '</li>').addClass("list-group-item"));
                $('#locations').append($('<li>Longitude: ' + json.longitude + '</li>').addClass("list-group-item"));
            }
        })
    });
});






