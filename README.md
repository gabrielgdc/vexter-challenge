# WEBSITE LOCATOR

### COMO FOI FEITO

Esse site foi feito utilizando jQuery e pra estilizar usei o framework Bootstrap, e a API usada foi a do IPSTACK

### EXECUÇÃO

Só abrir o index.html e testar o site não precisa compilar e nem servidor

### ESTRUTURA

1. Index.html - A parte com a estrutura da página, o que vai aparecer na tela pro usuário
2. Main.js - A parte lógica com todas as requisições para API e funções
3. Style.css - Um pouquinho de coisas que eu mudei fora do bootstrap para a página ( eu chamei o bootstrap e o jquery via cdn no arquivo index.html )

